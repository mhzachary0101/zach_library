﻿
AHK_OnScriptLoad()

OnExit("ExitFunc")

ExitFunc(ExitReason, ExitCode) {
    if ExitReason not in Logoff,Shutdown
        AHK_ReleaseKeys()
}

AHK_OnScriptLoad() {
    AHK_ReleaseKeys()
}

AHK_ReleaseKeys() {
    send {LShift up}
    send {RShift up}
    send {LCtrl  up}
    send {RCtrl  up}
    send {LWin   up}
    send {RWin   up}
    send {LAlt   up}
    send {RAlt   up}
}

SwitchHotkeyPackage(PackageName) {
    ;MsgBox %PackageName%
    file := FileOpen("HotkeyPackage\00_PackageSwitch.ahk", "w")
    if !IsObject(file) {
        MsgBox Can't open "%FileName%" for writing.
        return
    }
    file.Write(PackageName)
    file.Close()
}