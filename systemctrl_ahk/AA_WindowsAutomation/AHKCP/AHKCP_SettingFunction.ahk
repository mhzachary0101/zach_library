﻿
CPLoadSettings()

;==========================================================
; Load Command Picker Settings From File.
;==========================================================
CPLoadSettings()
{
	; Include any global setting variables the we need.
	global _cpSettingsFileName, _cpWindowWidthInPixels, _cpNumberOfCommandsToShow, _cpShowAHKScriptInSystemTray, _cpShowSelectedCommandWindow, _cpCommandMatchMethod, _cpNumberOfSecondsToShowSelectedCommandWindowFor, _cpShowSelectedCommandWindowWhenInfoIsReturnedFromCommand, _cpNumberOfSecondsToShowSelectedCommandWindowForWhenInfoIsReturnedFromCommand, _cpEscapeKeyShouldReloadScriptWhenACommandIsRunning
	
	; If the file exists, read in its contents and then delete it.
	If (FileExist(_cpSettingsFileName))
	{
		; Read in each line of the file.
		Loop, Read, %_cpSettingsFileName%
		{
			; Split the string at the = sign
			StringSplit, setting, A_LoopReadLine, =
			
			; If this is a valid setting line (e.g. setting=value)
			if (setting0 = 2)
			{
				; Get the setting variable's value
				_cp%setting1% = %setting2%
			}
		}
	}

	; Save the settings.
	CPSaveSettings()
	
	; Apply any applicable settings.
	CPShowAHKScriptInSystemTray(_cpShowAHKScriptInSystemTray)
}

;==========================================================
; Save Command Picker Settings To File.
;==========================================================
CPSaveSettings()
{
	; Include any global setting variables the we need.
	global _cpSettingsFileName, _cpWindowWidthInPixels, _cpNumberOfCommandsToShow, _cpShowAHKScriptInSystemTray, _cpShowSelectedCommandWindow, _cpCommandMatchMethod, _cpNumberOfSecondsToShowSelectedCommandWindowFor, _cpShowSelectedCommandWindowWhenInfoIsReturnedFromCommand, _cpNumberOfSecondsToShowSelectedCommandWindowForWhenInfoIsReturnedFromCommand, _cpEscapeKeyShouldReloadScriptWhenACommandIsRunning
	
	; Delete and recreate the settings file every time so that if new settings were added to code they will get written to the file.
	If (FileExist(_cpSettingsFileName))
	{
		FileDelete, %_cpSettingsFileName%
	}
	
	; Write the settings to the file (will be created automatically if needed)
	; Setting name in file should be the variable name, without the "_cp" prefix.
	FileAppend, CPShowAHKScriptInSystemTray=%_cpShowAHKScriptInSystemTray%`n, %_cpSettingsFileName%
	FileAppend, WindowWidthInPixels=%_cpWindowWidthInPixels%`n, %_cpSettingsFileName%
	FileAppend, NumberOfCommandsToShow=%_cpNumberOfCommandsToShow%`n, %_cpSettingsFileName%
	FileAppend, CommandMatchMethod=%_cpCommandMatchMethod%`n, %_cpSettingsFileName%
	FileAppend, ShowSelectedCommandWindow=%_cpShowSelectedCommandWindow%`n, %_cpSettingsFileName%
	FileAppend, NumberOfSecondsToShowSelectedCommandWindowFor=%_cpNumberOfSecondsToShowSelectedCommandWindowFor%`n, %_cpSettingsFileName%
	FileAppend, ShowSelectedCommandWindowWhenInfoIsReturnedFromCommand=%_cpShowSelectedCommandWindowWhenInfoIsReturnedFromCommand%`n, %_cpSettingsFileName%
	FileAppend, NumberOfSecondsToShowSelectedCommandWindowForWhenInfoIsReturnedFromCommand=%_cpNumberOfSecondsToShowSelectedCommandWindowForWhenInfoIsReturnedFromCommand%`n, %_cpSettingsFileName%
	FileAppend, EscapeKeyShouldReloadScriptWhenACommandIsRunning=%_cpEscapeKeyShouldReloadScriptWhenACommandIsRunning%`n, %_cpSettingsFileName%
}

/*
;==========================================================
; Add a Dummy command to use for debugging.
;==========================================================
AddNamedCommand("Dummy Command", "DummyCommand", "A command that doesn't do anything, but can be useful for testing and debugging", "Parameter1Name|Parameter1Value, Parameter2Value,Param3Name|Param3Value,Param4Value")
DummyCommand(parameters = "")
{
	; Example of how to check if parameters were provided.
	if (parameters != "")
		MsgBox, Parameters were provided!
	
	; Example of how to loop through the parameters
	Loop, Parse, parameters, CSV
		MsgBox Item %A_Index% is '%A_LoopField%'
	
	return, "This is some text returned by the dummy command."
}
*/

;==========================================================
; Inserts the scripts containing the commands (i.e. string + function) to run.
; The scripts we are including should add commands and their associated functions.
;
; Example:
;	AddCommand("SQL", "Launch SQL Management Studio")
;	SQL()
;	{
;		Run "C:\Program Files (x86)\Microsoft SQL Server\100\Tools\Binn\VSShell\Common7\IDE\Ssms.exe"
;		return false	; (optional) Return false to not display the command text after running the command.
;	}
;==========================================================