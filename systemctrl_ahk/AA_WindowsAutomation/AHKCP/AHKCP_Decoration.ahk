﻿;================================================================
; Global Variables - prefix everything with "cp" for Command Picker, so that variable/function names are not likely to conflict with user variables/function names.
;================================================================
_cpWindowName := "AHK Command Picker v1.3.1"
_cpWindowGroup := ""					; The group that will hold our Command Picker window so we can reference it from # directive statements (e.g. #IfWinExists).
_cpCommandList := ""					; Will hold the list of all available commands.
_cpCommandSelected := ""				; Will hold the command selected by the user.
_cpSearchedString := ""					; Will hold the actual string that the user entered.
_cpCommandArray := Object()				; Will hold the array of all Command objects.
_cpCommandDelimiter := "|"				; The character used to separate each command in the _cpCommandList. This MUST be the pipe character in order to work with a ListBox/ComboBox/DropDownList/Tab.
_cpCommandIsRunning := false			; Tells if a Command is currently being executed or not.

; Delimeters/Separators seen or used by the user.
_cpParameterDelimiter := ","			; The character used to separate each parameter in the AddCommand() function's parameter list and when manually typing in custom parameters into the search box. Also used to separate and each command in the AddCommands() function's command list. This MUST be a comma for the Regex whitespace removal to work properly, and it makes it easy to loop through all of the parameters using CSV.
_cpCommandParameterListSeparator := ","	; The character used to separate the command name from the parameter list when the user is typing their command.
_cpCommandDescriptionSeparator := "=>"	; The character or string used to separate the command name from the description of what the command does.
_cpCommandParameterSeparator := ","		; The character or string used to separate the command name from the command's preset parameter name/value in the Listbox.
_cpParameterNameValueSeparator := "|"	; The character used to separate a preset parameter's name from its value, in the AddCommand() function's parameter list.
_cpCommandNameValueSeparator := "|"		; The character used to separate a command's name from its value, in the AddCommands() function's command list.

;----------------------------------------------------------
; Global Variables Used By The User In Their Code.
;----------------------------------------------------------
_cpActiveWindowID := ""					; Will hold the ID of the Window that was Active when this picker was launched.
_cpDisableEscapeKeyScriptReloadUntilAllCommandsComplete := false	; Variable that user can set to True to disable the Escape key from reloading the script.

;----------------------------------------------------------
; AHK Command Picker Settings - Specify the default Command Picker Settings, then load any existing settings from the settings file.
;----------------------------------------------------------
_cpSettingsFileName := "AHKCP\AHKCommandPicker.settings"
_cpShowAHKScriptInSystemTray := true
_cpWindowWidthInPixels := 700
_cpFontSize := 10
_cpNumberOfCommandsToShow := 20
_cpCommandMatchMethod := "Type Ahead"	; Valid values are: "Type Ahead" and "Incremental".
_cpShowSelectedCommandWindow := true
_cpNumberOfSecondsToShowSelectedCommandWindowFor := 2.0
_cpShowSelectedCommandWindowWhenInfoIsReturnedFromCommand := true
_cpNumberOfSecondsToShowSelectedCommandWindowForWhenInfoIsReturnedFromCommand := 4.0
_cpEscapeKeyShouldReloadScriptWhenACommandIsRunning := true
;================================================================