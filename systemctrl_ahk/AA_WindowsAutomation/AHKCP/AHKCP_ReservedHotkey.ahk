﻿
;==========================================================
; Hotkey to reload the AHK Command Picker when it is executing a command.
; This can be used if one of the scripts is out of control and you need to kill it quickly.
;==========================================================
Esc Up::
	global _cpEscapeKeyShouldReloadScriptWhenACommandIsRunning, _cpCommandIsRunning, _cpDisableEscapeKeyScriptReloadUntilAllCommandsComplete
	
	; Rethrow the Escape keypress that we intercepted.
	SendInput, {Esc}
	
	; If the Escape key is allowed to reload the script when a Command is running, AND a Command is running, AND the user hasn't temporarily disabled the Escape key refresh functionaliry, then reload the script.
	if (_cpEscapeKeyShouldReloadScriptWhenACommandIsRunning && _cpCommandIsRunning && !_cpDisableEscapeKeyScriptReloadUntilAllCommandsComplete)
		Run, %A_ScriptFullPath%		
return


; Use Alt+Left Mouse Button to move a window
Alt & LButton::
                CoordMode, Mouse  ; Go by absolute coordinates
                MouseGetPos, MouseStartX, MouseStartY, MouseWin ; Get the mouses' starting position within the window, as an anchor.
                SetTimer, WatchMouse, 10 ; Track the mouse as it is dragged, and run the WatchMouse event every 10 ms.

WatchMouse:
                GetKeyState, LButtonState, LButton, P
                if LButtonState = U  ; If Left Mouse Button is released, stop dragging
                {
                                SetTimer, WatchMouse, off
                                return
                }
                ; Otherwise, reposition the window as the mouse moves
                CoordMode, Mouse ; Get coordinates relative to the Mouse
                MouseGetPos, MouseX, MouseY ; Get current mouse X and Y
                WinGetPos, WinX, WinY,,, ahk_id %MouseWin% ; Get current window X and Y, and the ID of the window that must be moved
                SetWinDelay, -1   ; Makes the move faster/smoother
                NewWinX := WinX + MouseX - MouseStartX
                NewWinY := WinY + MouseY - MouseStartY
                WinMove, ahk_id %MouseWin%,, %NewWinX%, %NewWinY% ; Move the window by ID to the new location
                MouseStartX := MouseX  ; Set a new mouse anchor.
                MouseStartY := MouseY
return

; Launch AHK_CommandPicker window
LShift & CapsLock::
    if GetKeyState(CapsLock, "T") = 1
        SetCapslockState, On
    else if GetKeyState(CapsLock, "T") = 0
        SetCapslockState, Off
	CPLaunchCommandPicker()	
return