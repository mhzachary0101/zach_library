﻿
AddCommand("File_ExcelAddIn", "")
File_ExcelAddIn() {
	Run, C:\Users\zach\AppData\Roaming\Microsoft\AddIns
}

AddCommand("File_WinAuto", "E:\U_PortableAppsData\AA_WinSystemCtrl")
File_WinAuto() {
	Run, E:\U_PortableAppsData\AA_WinSystemCtrl
}

AddCommand("File_Library", "E:\C_Library")
File_Reference() {
	Run, E:\C_Library
}

AddCommand("File_C", "C:\")
File_C() {
	Run, explore C:\
}

AddCommand("File_RecycleBin", "")
File_RecycleBin() { 
	Run, ::{645ff040-5081-101b-9f08-00aa002f954e}  ; Opens the Recycle Bin.
}

AddCommand("File_ThisComputer", "")
File_Root() { 
	Run, ::{20d04fe0-3aea-1069-a2d8-08002b30309d}  ; Opens the "My Computer" folder.
}