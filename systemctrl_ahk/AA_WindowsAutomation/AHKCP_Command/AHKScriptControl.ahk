﻿

AddCommand("AHK_ReloadSession", "")
AHK_ReloadSession() {
    Reload
    ;Run, %A_ScriptFullPath%
}

AddCommand("KEY_Excel", "")
KEY_Excel() {
    SwitchHotkeyPackage("#Include HotkeyPackage\excel.ahk")
    AHK_ReloadSession()
}

AddCommand("KEY_Generic", "")
KEY_Generic() {
    SwitchHotkeyPackage("#Include HotkeyPackage\generic.ahk")
    AHK_ReloadSession()
}

AddCommand("AHK_PauseSession", "")
AHK_PauseSession() {
	Pause
}

AddCommand("AHK_ExitSession", "")
AHK_ExitSession() {
	ExitApp
}