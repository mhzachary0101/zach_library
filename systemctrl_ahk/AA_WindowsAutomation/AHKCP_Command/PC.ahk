﻿
AddCommand("PC_Lock", "")
PC_Lock() {
    DllCall("LockWorkStation")
}

AddCommand("PC_ScreenOff", "")
PC_ScreenOff() {
	Sleep 100 ; if you use this with a hotkey, not sleeping will make it so your keyboard input wakes up the monitor immediately.
	SendMessage 0x112, 0xF170, 2,,Program Manager ; send the monitor into standby (off) mode.
}

AddCommand("PC_LockAndScreenOff", "")
PC_LockAndScreenOff() {
	Sleep 100 ; if you use this with a hotkey, not sleeping will make it so your keyboard input wakes up the monitor immediately.
    DllCall("LockWorkStation")
	SendMessage 0x112, 0xF170, 2,,Program Manager ; send the monitor into standby (off) mode.
}

AddCommand("PC_Hibernate", "")
PC_Hibernate() {
    Run, %A_ScriptDir%\ExternalScript\PC_Hibernate.cmd
}

AddCommand("PC_Sleep", "")
PC_Sleep() {
    Run, %A_ScriptDir%\ExternalScript\PC_Sleep.cmd
}

AddCommand("PC_Restart", "")
PC_Restart() {
	Run, shutdown.exe -r -t 00
}

AddCommand("PC_Shutdown", "")
PC_Shutdown() {
	Run, shutdown.exe -s -t 00
}