﻿
AddCommand("g", "")
g(queries = "") {
	; If queries were supplied, run them.
	if (queries != "")
		querySupplied := DoWebSearch(queries)
	; Otherwise the user didn't supply a query, so just open the browser up to Google.
	else
		Run, www.google.com
}

AddCommand("Ping_Google", "")
Ping_Google() {
    Run, %A_ScriptDir%\ExternalScript\Ping_Address.cmd
}

AddCommand("Ping_Baidu", "")
Ping_Baidu() {
    Run, %A_ScriptDir%\ExternalScript\Ping_Address.cmd "www.baidu.com"
}