﻿
; https://www.makeuseof.com/tag/toggle-touchscreen-windows-10/
AddCommand("touch_on", "")
touch_on() {
    Run, %A_ScriptDir%\ExternalScript\SYS_TouchToggle_On.cmd
}

AddCommand("touch_off", "")
touch_off() {
    Run, %A_ScriptDir%\ExternalScript\SYS_TouchToggle_Off.cmd
}

AddCommand("clipboard", "")
clipboard(displayLengthInSeconds = 0)
{
	MsgBox, , Clipboard Text (other content such as images are not shown here), %Clipboard%, %displayLengthInSeconds%
}

AddCommand("taskmgr", "")
taskmgr() {
    Run, taskmgr
}

/*
AddCommand("PWR_LIST", "")
PWR_LIST() {
    Run, D:\E_MSWIN\EA_MSWIN_Auto\ExternalScript\PWR_PowerConfigList.cmd
}

AddCommand("PWR_Normal", "")
PWR_Normal() {
    Run, powercfg /S dae1f9ec-df3a-4b8c-aa74-e3fc8dcd7ec5
    PWR_LIST()
}

AddCommand("PWR_DoNotSleep", "")
PWR_DoNotSleep() {
    Run, powercfg /S 897b8d27-40ad-4bad-8215-fc610017b087
    PWR_LIST()
}
*/
AddCommand("powershell", "")
powershell() {
    Run, powershell.exe
}

AddCommand("cmd", "")
cmd(arg = "") {
    if (arg == "") 
        arg := "D:\A_Workspace"
    Run, cmd.exe, %arg%
}

AddCommand("ConEmu", "")
ConEmu() {
    Run, C:\Users\zach\PortableApps\ConEmu\ConEmu64.exe
}