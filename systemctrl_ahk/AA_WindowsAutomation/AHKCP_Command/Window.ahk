﻿
AddNamedCommand("WIN_CloseAll", "")
WIN_CloseAll() {
	MatchList = AutoHotKey Help, Any Other Window Names To Leave Open

	WinGet, ID, List, , , Program Manager
	Loop, %ID%
	{
		StringTrimRight, This_ID, ID%A_Index%, 0
		WinGetTitle, This_Title, ahk_id %This_ID%
		If This_Title in %MatchList%
			Continue
		WinClose, %This_Title%
	}
}

AddCommand("WIN_Min", "")
WIN_Min() {	
    global _cpActiveWindowID
	WinMinimize, ahk_id %_cpActiveWindowID%
}

AddCommand("WIN_Max", "")
WIN_Max() {	
    global _cpActiveWindowID
	WinMaximize, ahk_id %_cpActiveWindowID%
}

AddCommand("WIN_AlwaysOnTop_On", "")
WIN_AlwaysOnTop_On() {	
    global _cpActiveWindowID
	WinSet, AlwaysOnTop, On, ahk_id %_cpActiveWindowID%
}

AddCommand("WIN_AlwaysOnTop_Off", "")
WIN_AlwaysOnTop_Off() {	
    global _cpActiveWindowID
	WinSet, AlwaysOnTop, Off, ahk_id %_cpActiveWindowID%
}
