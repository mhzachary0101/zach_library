﻿; show hotkey package
~LCtrl::
    if (A_PriorHotkey <> "~LCtrl" or A_TimeSincePriorHotkey > 400) {
        ; Too much time between presses, so this isn't a double-press.
        KeyWait, LCtrl
        return
    }
    MsgBox, Generic Hotkey Package
    AHK_ReloadSession()
return