﻿; show hotkey package   // LCtrl & LCtrl
; paste value           // LShift & LCtrl & v
; paste format          // LShift & LCtrl & c
; paste formulas        // LShift & LCtrl & x
; redo                  // LShift & LCtrl & z
; replace window        // LShift & LCtrl & f

; show hotkey package
~LCtrl::
    if (A_PriorHotkey <> "~LCtrl" or A_TimeSincePriorHotkey > 200) {
        ; Too much time between presses, so this isn't a double-press.
        KeyWait, LCtrl
        return
    }
    MsgBox, Excel Hotkey Package
    AHK_ReloadSession()
return

; paste formulas
LShift & x::
    If GetKeyState("LCtrl") {
        send {Ctrl down}{Alt down}v
        send {Ctrl up}{LAlt up}
        send f{Enter}
        return
    } else {
        send X
    }
return

; paste format
LShift & c::
    If GetKeyState("LCtrl") {
        send {Ctrl down}{Alt down}v
        send {Ctrl up}{LAlt up}
        send t{Enter}
        return
    } else {
        send C
    }
return

; paste value
LShift & v::
    If GetKeyState("LCtrl") {
        send {Ctrl down}{Alt down}v
        send {Ctrl up}{LAlt up}
        send v{Enter}
    } else {
        send V
    }
return

; redo
LShift & z::
    If GetKeyState("LCtrl") {
        send {LShift down}{F10 down}f
        send {LShift up}{F10 up}
    } else {
        send Z
    }
return

; replace window
LShift & f::
    If GetKeyState("LCtrl") {
        send {Ctrl down}f
        send {Ctrl up}
        send {Alt down}r
        send {Alt up}
    } else {
        send F
    }
return
