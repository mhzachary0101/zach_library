﻿
#SingleInstance force	; Make it so only one instance of this script can run at a time (and reload the script if another instance of it tries to run).
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases. 
        ; Avoid checking empty variables to see if they are environment variables (better performance).

SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.

; #Warn     ; Enable warnings to assist with detecting common errors.
            ; Causing Error to AHK_CommandPicker

; SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#Include AHK_Function\HotkeyPackage_Function.ahk

#Include AHKCP\AHKCP_Decoration.ahk
#Include AHKCP\AHKCP_SettingFunction.ahk
#Include AHKCP\AHKCP_Function.ahk

#Include AHKCP_Command\AHKScriptControl.ahk
#Include AHKCP_Command\Application.ahk
#Include AHKCP_Command\FileSystem.ahk
#Include AHKCP_Command\PC.ahk
#Include AHKCP_Command\System.ahk
#Include AHKCP_Command\window.ahk
#Include AHKCP_Command\Workspace.ahk
#Include AHKCP_Command\WWW.ahk

#Include AHKCP\AHKCP_ReservedHotkey.ahk

#Include AHKCP\AHK_CommandPicker.ahk

#Include HotkeyPackage\00_PackageSwitch.ahk